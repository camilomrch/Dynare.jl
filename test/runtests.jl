include("checkpath.jl")

# Get current directory.
rootdir0 = @__DIR__

using Test

# Run tests
@testset "Dynare.jl testsuite" begin
    include("$(rootdir0)/preprocessor/test-1.jl")
    include("$(rootdir0)/preprocessor/test-2.jl")
    include("$(rootdir0)/preprocessor/test-3.jl")
    include("$(rootdir0)/preprocessor/test-4.jl")
    include("$(rootdir0)/dynare-solvers/test-1.jl")
    include("$(rootdir0)/dynare-solvers/test-2.jl")
    include("$(rootdir0)/steady-state/test-1.jl")
    include("$(rootdir0)/steady-state/test-2.jl")
end
