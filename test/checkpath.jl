# Put Dynare's sources in the path.
if isempty(findall((in)(LOAD_PATH), [abspath("$(@__DIR__)/../src")]))
    pushfirst!(LOAD_PATH, abspath("$(@__DIR__)/../src"))
end
